import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { fromJS } from 'immutable';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import configStore from 'setup/store';

const initialState = fromJS({});

const store = configStore(initialState);

const renderApp = (Component) => {
    
    render((<Provider store={store}>
               <Component/>
            </Provider>), 
            document.getElementById('root'));
};

renderApp(App);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
