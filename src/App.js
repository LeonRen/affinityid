import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import ProfilePage from "views/ProfilePage";

const App = () => {
    
    return(<div>
      <ProfilePage />
    </div>);
};

export default App;
