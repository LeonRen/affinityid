import imagesStyles from 'assets/jss/imagesStyles';
import { title } from 'assets/jss/material-kit-react';

export const profileUpdateStyle = theme => ({
    card: {
      width:"100%",
      maxWidth: 450,
      backgroundColor: theme.palette.background.paper,
      margin: '25%'
  },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      marginTop:"16px"
    }
});

export const confirmTabStyle = theme => ({
    transBox:{
      margin: "30px",
      backgroundColor:"#DC143C",
      border: "1px solid black",
      opacity: "0.6",
      filter: "alpha(opacity=60)", /* For IE8 and earlier */
      borderRadius: "10px"
    }
});

export const profileEditStyle = theme => ({
    profile: {
      textAlign: "center",
      "& img": {
        maxWidth: "160px",
        width: "100%",
        margin: "0 auto",
        transform: "translate3d(0, -50%, 0)"
      }
    },
    description: {
      margin: "1.071rem auto 0",
      maxWidth: "600px",
      color: "#999",
      textAlign: "center !important"
    },
    ...imagesStyles,
    main: {
      background: "#FFFFFF",
      position: "relative",
      zIndex: "3"
    },
    mainRaised: {
        margin: "104px -104px 0px",
        borderRadius: "6px",
        boxShadow:
          "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
      },
    container: {
        paddingRight: "15px",
        //paddingLeft: "15px",
        marginRight: "auto",
        marginLeft: "auto",
        width: "100%",
        "@media (min-width: 576px)": {
            maxWidth: "540px"
          },
          "@media (min-width: 768px)": {
            maxWidth: "720px"
          },
          "@media (min-width: 992px)": {
            maxWidth: "960px"
          },
          "@media (min-width: 1200px)": {
            maxWidth: "1140px"
          }
    },
    margin5: {
        margin: "5px"
      },
      title: {
        fontSize: "24px",
        color: "#4A4A4A",
        fontFamily: "Arial",
        fontWeight: "Bold",
        display: "inline-block",
        position: "relative",
        marginTop: "-50px",
        minHeight: "32px",
        textDecoration: "none"
      },
      email: {
        fontSize: "12px",
        color: "#9B9B9B",
        fontFamily: "Arial",
        lineHeight: "14px",
      }
      ,
      title: {
        fontSize: "12px",
        color: "#827E99",
        fontFamily: "Arial",
        fontWeight: "Bold"

    },
    description: {
      fontSize: "14px",
      color: "#9B9B9B",
      fontFamily: "Arial",
  },
  awesomeIcon: { 
    display: "inline-block",
    borderRadius: "60px",
    boxShadow: "0px 0px 2px #888",
    padding: "14px 14px", 
    margin:"5px 5px 5px 5px"
  }
    
});

export const profileListStyle = theme => ({
    section: {
      padding: "30px 10px 10px 10px",
      margin: "10px 10px 10px 10px",
      textAlign: "center",
      
    },
   avatar: {
        margin: 10,
    },
    bigAvatar: {
        margin: 10,
        width: 60,
        height: 60,
    },
    list: {
        width:"100%",
        maxWidth: 450,
        backgroundColor: theme.palette.background.paper,
        padding: "10px 20px 10px 20px"
    },
    card: {
        width:"100%",
        maxWidth: 450,
        backgroundColor: theme.palette.background.paper,
        margin: '25%'
    },
    title: {
        fontSize: 15,
        color: "#827E99",
        lineHeight: "18px",
        fontFamily: "Arial",
        fontWeight: "Bold"

    },
    subTitle: {
        fontSize: 14,
        color: "#827E99",
        lineHeight: "18px",
        fontFamily: "Arial",
        
    },
    description: {
        fontSize: 12,
        color: "#9B9B9B",
        lineHeight: "14px",
        fontFamily: "Arial",
    }
  });

