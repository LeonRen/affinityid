import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { Fab, Card, TextField } from "@material-ui/core";
// Core Component
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
// Style
import { profileUpdateStyle } from './style';

const inputObj = {

    profile: "Profile image",
    name: "Name",
    email: "Email address",
    role: "Role",
    team: "Team",
    address: "Address"
}

class ProfileUpdate extends React.Component {
  
  constructor(props)
  {
    super(props);
  }
  
  render() {
    const { classes, ...rest } = this.props;
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgFluid
    );

    const inputKeys = Object.keys(inputObj);
    return (

        <GridContainer className={classNames(classes.main, classes.mainRaised)}>
        <GridItem className={classes.container}>
        <Card className = {classes.card}>
        {
            inputKeys.map(inputKey=>   <TextField
                id={`filled-${inputObj[inputKey]}`}
                label={inputObj[inputKey]}
                className={classes.textField}
                variant="filled"
              />)
        }
        </Card>  
        </GridItem>
      </GridContainer>
       
    );
  }
}

export default withStyles(profileUpdateStyle)(ProfileUpdate);
