import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { Avatar, Card, CardHeader, CardContent, Typography, List, ListItem, ListItemText, ListItemAvatar } from "@material-ui/core";
// Redux
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
// Style
import { profileListStyle } from './style';
// Action, selector
import { getUsers, fetchUsers } from 'reducers/userEndPoint/action';
import { usersInfoSelector } from 'reducers/userEndPoint/selector';

const mapStateToProps = (state) => ({
  users: usersInfoSelector(state),
});

const mapDispatchToProps = (dispatch) => ({  
  getUsers: () => { dispatch(fetchUsers());}
});

class ProfileList extends React.Component {
   
    static propTypes = {
      users: ImmutablePropTypes.map,
      getUsers: PropTypes.func
    };

    static defaultProps = {
      users: Map(),
      getUsers: () => {}

    }

    componentDidMount(){
      console.log("get user")
      this.props.getUsers();
    }
  
    render() {
      const { classes, users } = this.props;

      return (
              <div className={classes.section}>
              {
                users.map(user =>(<Card className = {classes.card}>
                  <List className ={classes.list}>
                  <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                        <Avatar alt={user.userName} src={user.photo}  className ={classes.bigAvatar}/>
                  </ListItemAvatar>
                  <ListItemText
                primary={
                  <Typography component="span" classes={{root:classes.title}} color="textPrimary">
                    {user.name}
                  </Typography>
                }
                secondary={
                  <React.Fragment>
                    <Typography component="span" classes={classes.subTitle} color="textPrimary">
                      {user.role}
                    </Typography>
      
                    {<div className={classes.description}>{user.department}</div>}
                  </React.Fragment>
                }
              />
               </ListItem>
                  </List>
                  </Card>)
                )
              }
              </div>
      );
    }
  }
  
  export default withStyles(profileListStyle)(connect(mapStateToProps, mapDispatchToProps)(ProfileList));
  