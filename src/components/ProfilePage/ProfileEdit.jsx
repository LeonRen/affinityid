import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { Fab } from "@material-ui/core";
// @fortawesome component
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
// @fortawesome/icons
import { faPen, faTimes } from "@fortawesome/free-solid-svg-icons";
//Core component
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import ConfirmTab from './ConfirmTab'; 
//Style
import { profileEditStyle } from './style';



class ProfileEdit extends React.Component {
  
  constructor(props)
  {
    super(props);

    this.state ={ isConfirm:false};
  }
  
  render() {
    const { classes, ...rest } = this.props;
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgFluid
    );
    return (

        <GridContainer className={classNames(classes.main, classes.mainRaised)}>
        <GridItem className={classes.container}>
         
                <div className={classes.profile}>
                  <div>
                    <img src="assets/img/andy.png" alt="..." className={imageClasses} />
                  </div>
                  <div>
                    <div className={classes.title} >Christian Louboutin</div>
                    <div className={classes.email}>DESIGNER</div>
                  </div>
                </div>
        </GridItem>
        <GridItem style={{textAlign:"center"}}>
          {this.state.isConfirm?(<ConfirmTab>
          <FontAwesomeIcon className={classes.awesomeIcon} icon={faPen}></FontAwesomeIcon>
          <a onClick={()=>{alert("test")}}><FontAwesomeIcon className={classes.awesomeIcon} icon={faTimes}></FontAwesomeIcon></a>
          </ConfirmTab>):(<div> <a onClick={()=>{this.setState({isConfirm:true})}}><FontAwesomeIcon className={classes.awesomeIcon} icon={faPen}></FontAwesomeIcon></a>
          <FontAwesomeIcon className={classes.awesomeIcon} icon={faTimes}></FontAwesomeIcon></div>)
          }
        </GridItem>
        <GridItem>
          <hr />
          <GridContainer>
          <GridItem xs={6} sm={6} md={6}>
          <div>{"Role"}</div>
          <div>{"Aadmin"}</div>
         </GridItem>
         <GridItem xs={6} sm={6} md={6}>
         <div>{"Team"}</div>
         <div>{"Creative"}</div>
         </GridItem>
         </GridContainer>
        </GridItem>
        <GridItem>
          <hr />
          <GridContainer>
          <GridItem xs={6} sm={6} md={6}>
          <div >{"Role"}</div>
          <div >{"Admin"}</div>
         </GridItem>
         <GridItem xs={6} sm={6} md={6}>
         <div >{"Team"}</div>
         <div >{"Creative"}</div>
         </GridItem>
         </GridContainer>
        </GridItem>
        <GridItem style={{textAlign:"center", margin:"10px 10px 10px 10px"}}>
          <Fab
             variant="extended"
             size="large"
             color="default"
             aria-label="Add"
             style={
               {
                 width:"100%"
               }
             }
             >
           <span style={{textColor:"#9B9B9B"}}>SHARE</span>
           </Fab> 
        </GridItem>
      </GridContainer>
       
    );
  }
}

export default withStyles(profileEditStyle)(ProfileEdit);
