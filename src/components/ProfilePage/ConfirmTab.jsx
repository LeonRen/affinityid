import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { Button } from "@material-ui/core";

//Style
import { confirmTabStyle } from './style';


class ConfirmTab extends React.Component {

          
    render() {
        
        const { classes, children } = this.props;

        return (
            <div className={classes.transBox}>
            {children}
            </div>
        )
    }

}

export default withStyles(confirmTabStyle)(ConfirmTab);