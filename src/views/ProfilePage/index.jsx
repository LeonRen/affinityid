import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { Fab } from '@material-ui/core';
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import ProfileList from 'components/ProfilePage/ProfileList';
import ProfileEdit from 'components/ProfilePage/ProfileEdit';
import ProfileUpdate from 'components/ProfilePage/ProfileUpdate';

const profileStyle = theme => ({
    section: {
      backgroundImage:"url('assets/img/background.png')",
      maxWidth: 1300
    },
    button: {
        color:"#ffffff",
        margin: theme.spacing.unit,
        backgroundColor: "#212121",
        width:"100%"
    },
  });

class ProfilePage extends React.Component {

  constructor(props){
      
    super(props);
    this.state = {showUpdate:false};
  }
  
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={6}>
             <div style={{textAlign:"center"}}>
             <h1>
              AFFINITY ID
             </h1>
             </div>
             <ProfileList />

          </GridItem>
          <GridItem xs={12} sm={12} md={6} >
           <GridContainer>
             <GridItem>
           <Fab
             variant="extended"
             size="large"
             color="default"
             aria-label="Add"
             classes={
                 {root: classes.button}
             }
             onClick={()=>{ this.setState({showUpdate:this.state.showUpdate?false:true})}}
             >
           { this.state.showUpdate?"Cancal Creation":"CREATE A NEW EMPLOYEE"}
           </Fab>
           </GridItem>
           <GridItem style={{padding:"150px"}}>
           {
            this.state.showUpdate?<ProfileUpdate/>:<ProfileEdit />
           }
            </GridItem>  
           </GridContainer>

           
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(profileStyle)(ProfilePage);
