import { createSelector } from 'reselect'

const usersSelector = state => state.get('users');
const usersInfoSelector = createSelector([usersSelector], users => users.get('users'));

export {
	
	usersSelector, 
	usersInfoSelector,
}