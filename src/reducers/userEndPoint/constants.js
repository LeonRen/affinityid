export const GET_USERS = '/users/GET';
export const GET_USERS_SUCCESS = '/users/GET/SUCCESS';
export const GET_USERS_FAILURE = '/users/GET/FAILURE';
