import { createAction } from 'redux-actions';

import { GET_USERS, GET_USERS_SUCCESS } from './constants';
import { remoteRequest } from './remote';
import { API } from "setup/config";

export const getUsers = createAction(GET_USERS);
export const getUsersSuccess = createAction(GET_USERS_SUCCESS);

export const fetchUsers = function () {
   
    return (dispatch, getState) =>{
     
    let state = getState();

    const request = { 
    	method: 'get',
    	url: `${API}users`
};

	remoteRequest(request).then(res => {
		
		let action = getUsersSuccess();
		action.payload = res.data;
        dispatch(action);

	}).catch(err => {
		throw new Error(err);
	});
}
}