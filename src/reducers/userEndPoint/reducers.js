import { handleActions } from 'redux-actions';
import { fromJS } from 'immutable';

import { GET_USERS_SUCCESS } from './constants';


const initialState = fromJS({
    users:[]
});

export default handleActions({

	[GET_USERS_SUCCESS]: (state, action) => {
        
        console.log('Action payload -----------');
        console.log(action.payload);
        console.log('Action payload -----------')
		return state.set('users', action.payload);
	},
}, initialState);