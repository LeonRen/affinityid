import { combineReducers } from 'redux-immutable';

import userReducer from 'reducers/userEndPoint/reducers';

export default function createReducer() {

	return combineReducers(
	{
		users: userReducer
	});
};
