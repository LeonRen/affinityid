import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'

import createReducer from './reducer';


export default function configStore(initialState = {})
{
	const middlewares = [
	thunkMiddleware, 
	];

	const store = createStore(createReducer(), initialState, applyMiddleware(...middlewares));

	return store;
}